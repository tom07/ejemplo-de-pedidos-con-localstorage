import { Component, OnInit, SimpleChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProductosService } from '../services/productos.service';
import { Productos } from '../models/productos';

@Component({
  selector: 'app-agregar-productos',
  templateUrl: './agregar-productos.component.html',
  styleUrls: ['./agregar-productos.component.scss']
})
export class AgregarProductosComponent implements OnInit {
  formularioAgregar: FormGroup;
  tom: string = ''
  constructor(private fb: FormBuilder, public productosServicio: ProductosService) { }
  
  ngOnInit() {
    this.formularioAgregar = this.fb.group({
      nombre: ['', Validators.required],
      descripcion: ['', Validators.required],
      precio: ['', Validators.required]
    })
  }


  agregar()
  {
     
    this.productosServicio.agregarLocalStorage(this.formularioAgregar.value)
    this.formularioAgregar.reset();
  }

}
