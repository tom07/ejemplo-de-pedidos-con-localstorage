import { Component, OnInit } from '@angular/core';
import { PedidosService } from '../services/pedidos.service';

@Component({
  selector: 'app-encabeza',
  templateUrl: './encabeza.component.html',
  styleUrls: ['./encabeza.component.scss']
})
export class EncabezaComponent implements OnInit {

  constructor(private pedidosServicio: PedidosService) { }

  ngOnInit() {
    
  }

}
