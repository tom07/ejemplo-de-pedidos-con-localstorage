import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EncabezaComponent } from './encabeza.component';

describe('EncabezaComponent', () => {
  let component: EncabezaComponent;
  let fixture: ComponentFixture<EncabezaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EncabezaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EncabezaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
