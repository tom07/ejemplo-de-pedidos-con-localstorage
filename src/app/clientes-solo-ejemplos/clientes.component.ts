import { Component, OnInit } from '@angular/core';
interface Clientes{
  nombre:string;
  apellido:string;
  edad: number;
}

interface Productos{
  nombre: string;
  precio: number;
}
@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.scss']
})
export class ClientesComponent implements OnInit {
  constructor() { }

  ngOnInit() {
  }


  guardarCliente()
  {
    let clientesAgregar: Array<Clientes> = new Array<Clientes>();
    clientesAgregar.push({
      nombre: 'Maria',
      apellido:'Perez',
      edad: 10
    },
    {
      nombre: 'Ana',
      apellido: 'Martinez',
      edad: 25
    }
    )
    localStorage.setItem("clientes",JSON.stringify(clientesAgregar))
  }
  guardarProductos()
  {
    let productosAgregar: Array<Productos> = new Array<Productos>();
    productosAgregar.push({
      nombre: 'Maíz',
      precio: 25
    },
    {
      nombre: 'Agua en botella',
      precio: 35
    }
    )
    localStorage.setItem("productos",JSON.stringify(productosAgregar))
  }


  eliminarClientes()
  {
    localStorage.removeItem("clientes")
  }

  eliminarProductos(){
    localStorage.removeItem("productos")
  }

  eliminarTodos()
  {
    localStorage.clear();

  }

  get cLientesLocales(): Clientes[]{
    let clientesLocalStorage: Clientes[] = JSON.parse(localStorage.getItem("clientes"))
    if(clientesLocalStorage == null)
    {
      return new Array<Clientes>();
    }
    return clientesLocalStorage
  }

  get productosLocales(): Productos[]{
    let productosLocalStorage: Productos[] = JSON.parse(localStorage.getItem("productos"))
    if(productosLocalStorage == null)
    {
      return new Array<Productos>();
    }
    return productosLocalStorage
  }

}
