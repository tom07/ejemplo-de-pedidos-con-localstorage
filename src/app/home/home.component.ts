import { Component, OnInit, SimpleChanges } from '@angular/core';
import { ClientesService } from '../services/clientes.service';
import { Clientes } from '../models/clientes';
import { PedidosService } from '../services/pedidos.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  clientes: Array<Clientes> = new Array<Clientes>();
  nombre: string = 'Juan'
  constructor(public clientesServicio: ClientesService, public pedidosServicio: PedidosService, public route: Router) { }

  ngOnInit() {
    this.clientes = this.clientesServicio.clientesLocalStorage;
  }


  buscarClientes(nombreBuscar)
  {
   
    this.clientes = this.clientesServicio.clientesLocalStorage.filter(cliente=> {
      return cliente.nombre.toLocaleLowerCase().includes(nombreBuscar.toLocaleLowerCase() )
    })
  }

  irAProductos(cliente: Clientes)
  {
    this.pedidosServicio.pedido.clienteID = cliente.clienteID;
    this.pedidosServicio.pedido.nombreCliente = `${cliente.nombre} ${cliente.apellido}`;
    this.pedidosServicio.guardarLocalStorage();
    this.route.navigateByUrl("/productos")
  }

  ngOnChanges(changes: SimpleChanges)
  {
    console.log('Cuando el input de un componente cambia');
  }
  ngDoCheck()
  {
    console.log('Jola')
  }


} 
